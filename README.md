barrel-rabbitmq
=====

An OTP application which publishes events in Barrel Database to RabbitMQ.

The aim of this application is to publish events that occur in the Barrel Database (creation, update, and deletion of objects from the Barrel) via RabbitMQ message broker for consumption by other applications.

Requirements:
-----

- Running instance of [Barrel Database](https://barrel-db.org/).
- Running instance of [RabbitMQ](http://www.rabbitmq.com/).

Configuration:
-----

To configure the Barrel and RabbitMQ parameters to be used by this application make a copy of `sys.config.example` located in the root of the repo and rename it to `sys.config`.

Fill in the corresponding values [More Info to Add].

Building the Application:
-----

Make sure you have [Rebar3](rebar3.org) installed then run the following commands from the root of this repo:

```
rebar3 rel
```

Running the Application
-----

After starting Barrel and RabbitmQ:

To start publishing Barrel events to RabbitMQ:
```
_build/default/rel/barrel_rabbitmq/bin/barrel_rabbitmq start
```

To stop publishing Barrel events to RabbitMQ:

```
_build/default/rel/barrel_rabbitmq/bin/barrel_rabbitmq stop
```
